package gui;


import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import domZdravlja.DomZdravlja;
import net.miginfocom.swing.MigLayout;

public class IzaberiProzor extends JFrame {
	
	private JLabel lblPoruka1 = new JLabel("");
	private JLabel lblPoruka2 = new JLabel("Izaberite opciju.");
	private JLabel lblPrazno = new JLabel("");

	
	private JButton btnLekar = new JButton("Lekari");
	private JButton btnSestra = new JButton("M.Sestre");
	private JButton btnPacijent = new JButton("Pacijenti");
	
	private DomZdravlja domZdravlja;
	
	public IzaberiProzor(DomZdravlja domZdravlja) {
		this.domZdravlja = domZdravlja;
		setTitle("Opcije");
		setSize(850,450);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		initGUI();
		initListeners();

	}
	private void initGUI() {
		MigLayout mig = new MigLayout();
		setLayout(mig);
		lblPoruka1.setFont(new Font("American Typewriter",Font.PLAIN,25));
		lblPoruka2.setFont(new Font("Helvetica Neue",Font.PLAIN,25));


		add(lblPoruka1,"wrap 5");
		add(lblPoruka2,"wrap 50");

		add(lblPrazno,"wrap 25");
		add(btnLekar,"gapleft20");
		add(btnSestra,"gapleft15");
		add(btnPacijent,"gapleft100");
	}
	
	private void initListeners() {
		btnLekar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				LekarLoginProzor lekarLogin = new LekarLoginProzor(domZdravlja);
				lekarLogin.setVisible(true);
			}
		});
		btnSestra.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				MedicinskaSestraLoginProzor sestraLogin = new MedicinskaSestraLoginProzor(domZdravlja);
				sestraLogin.setVisible(true);
			}
		});
		btnPacijent.addActionListener(new ActionListener() {
	
			@Override
			public void actionPerformed(ActionEvent e) {
				PacijentLoginProzor pacijentLogin = new PacijentLoginProzor(domZdravlja);
				pacijentLogin.setVisible(true);
			}
		});
	}
}
